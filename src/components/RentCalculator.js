import React from 'react';

class RentCalculator extends React.Component {

    constructor() {
        super();

        this.state = {
            maxRent: 0,
        };
    }

    CalculateRent(event) {
        event.preventDefault();

        var income = this.income.value;
        var obligations = this.obligations.value;
        var capital = this.capital.value;

        var total = income / 4;

        this.setState({ maxRent: (total - obligations) + (capital * 0.1)});
    }

    render() {
        return (
            <div className="container">
                <form className="rent-calculator" onSubmit={(e) => this.CalculateRent(e)}>
                    <input ref={(input) => this.income = input}
                            type="number" 
                            required 
                            placeholder="Bruto inkomen per maand"/>
                    <input ref={(input) => this.obligations = input}
                            type="number" 
                            required 
                            placeholder="Maandelijkse verplichtingen"/>
                    <input ref={(input) => this.capital = input}
                            type="number" 
                            required 
                            placeholder="Eigen vermogen"/>
                    <button type="submit">Berekenen</button>
                </form>
                <div>&euro; {this.state.maxRent}</div>
            </div>
        )
    }
}

export default RentCalculator;