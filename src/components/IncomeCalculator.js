import React from 'react';

import { formatPrice } from '../helpers';

class IncomeCalculator extends React.Component {

    constructor() {
        super();

        this.state = {
            rent: 0,
            obligations: 0,
            capital: 0,
            minIncome: 0,
        };

        this.handleChange = this.handleChange.bind(this);
        this.CalculateIncome = this.CalculateIncome.bind(this);
    }

    handleChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    CalculateIncome(event) {
        event.preventDefault();

        var rent = this.rent.value;
        var obligations = this.obligations.value;
        var capital = this.capital.value;

        var total = rent * 4;

        this.setState({ minIncome: (total + obligations) - (capital * 0.1)});
    }

    render() {
        return (
            <div className="container">
                <form className="income-calculator" onSubmit={(e) => this.CalculateIncome(e)}>
                    <input ref={(input) => this.rent = input}
                            name="rent"
                            type="number" 
                            required 
                            placeholder="Huur per maand"
                            onChange={this.handleChange} />
                    <input ref={(input) => this.obligations = input}
                            name="obligations"
                            type="number" 
                            required 
                            placeholder="Maandelijkse verplichtingen"
                            onChange={this.handleChange} />
                    <input ref={(input) => this.capital = input}
                            name="capital"
                            type="number" 
                            required 
                            placeholder="Eigen vermogen"
                            onChange={this.handleChange}/>
                    <button type="submit">Berekenen</button>
                </form>
                <div>
                    <table>
                        <tr>
                            <td>Huur per maand:</td>
                            <td>{this.state.rent}</td>
                        </tr>
                        <tr>
                            <td>Maandelijkse verplichtingen</td>
                            <td>{this.state.obligations}</td>
                        </tr>
                        <tr>
                            <td>Eigen vermogen</td>
                            <td>{this.state.capital}</td>
                        </tr>
                        <tr className="total">
                            <td>Minimaal bruto inkomen</td>
                            <td>{formatPrice(this.state.minIncome)}</td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
}

export default IncomeCalculator;