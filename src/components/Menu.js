import React from 'react';
import { Link } from 'react-router';

class Menu extends React.Component {
    render() {
        return (
            <header className="masthead">
                <ul>
                    <li><Link to="/">Inkomen berekenen</Link></li>
                    <li><Link to="/rent">Huur berekenen</Link></li>
                </ul>
            </header>
        )
    }
}

export default Menu;