import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Match, Miss } from 'react-router';

import './css/style.css';
import Menu from './components/Menu';
import IncomeCalculator from './components/IncomeCalculator';
import RentCalculator from './components/RentCalculator';
import NotFound from './components/NotFound';

const Root = () => {
    return (
        <BrowserRouter basename="/calculator">
            <div>
                <Menu />
                <Match exactly pattern="/" component={IncomeCalculator} />
                <Match exactly pattern="/rent/" component={RentCalculator} />
                <Miss component={NotFound} />
            </div>
        </BrowserRouter>
    )
}

render(<Root/>, document.querySelector('#main'));